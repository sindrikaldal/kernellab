#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include "pidinfo.h"
#include <unistd.h>
#include <errno.h>

/*Error handling function*/
void unix_error(char *msg) {

	fprintf(stderr, "%s: %s\n", msg, strerror(errno));
	exit(0); 
}

/*Wrapper function for the write system call*/
ssize_t Write(int fd, const void *buf, size_t count) {
	
	size_t rc;
	
	if((rc = write(fd, buf, count)) < 0) {
	    unix_error("Write error");
	}
	return rc;
}

/*Wrapper function for the open system call*/
int Open(const char *pathname, int flags) {

	int rc;
	
	if((rc = open(pathname, flags)) < 0) {
	    unix_error("Open error");
	}
	
	return rc;
}


/* Part I */
void run_current()
{
        int pid = 0;

	/*Declare a pointer that points to the pid*/
	void* p = &pid;
	
	/*Retrieve te integer we get from opening the kcurrent file*/
	int file = Open("/sys/kernel/kernellab/kcurrent", O_WRONLY | O_APPEND);	
	
	/*Send the pointer to the kernel space and thus updating it*/
	write(file, &p, sizeof(int));
	
	/*Print out the current pid*/
        printf("Current PID: %d\n", pid);

	close(file);
}

/* Part II */
void run_pid()
{

	/* Declare a pid_info and sysfs_message and allocate memory for it.*/
        struct pid_info info;
	struct sysfs_message *message = malloc(sizeof(struct sysfs_message));

	/*Initalize the variables to dummy values in the pid_info that will be stored in the message*/
	info.pid = -1;
	strcpy(info.comm, "");
	info.state = -1;

	/*Initialize the variables in the message*/
	message->pid = getpid();
	message->address = &info;

	/*Retrieve the integer that we get from opening the pid file*/
	int file = Open("/sys/kernel/kernellab/pid", O_WRONLY | O_APPEND);	
	
	/* Write to pid file and send a pointer that points to the masse
 	 * and thus updating the variables in the message*/
	Write(file, (void *)&message, sizeof(int));

	/*Print out the variables*/
        printf("PID: %d\n", info.pid);
        printf("COMM: %s\n", info.comm);
        printf("State: %ld", info.state);

	/*Close the file that was opened*/
	close(file);

	/*Free the pointer*/
	free(message);
}



int main()
{
        run_current();    
        run_pid();
        return EXIT_SUCCESS;
}
