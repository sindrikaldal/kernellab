/*
 * Kernellab
 */
#include <linux/types.h>
#include <linux/sched.h>
#include <linux/uaccess.h>
#include <linux/kernel.h>
#include <linux/kobject.h>
#include <linux/string.h>
#include <linux/sysfs.h>
#include <linux/module.h>
#include <linux/init.h>
#include <linux/slab.h>
#include "pidinfo.h"

/* Part I */
static ssize_t kcurrent_write(struct kobject *kobj, struct kobj_attribute *attr,
			 const char *buf, size_t count)
{

	if(copy_to_user((void *)*(int *)buf, &current->pid, count) != 0) {
		return -EFAULT;
	}
	      			
  	return count;
}

static struct kobj_attribute kcurrent_attribute =
	__ATTR(kcurrent, 0220, NULL, kcurrent_write);


/* Part II */
static ssize_t pid_write(struct kobject *kobj, struct kobj_attribute *attr,
			 const char *buf, size_t count)
{


	/*Initalize a task_struct so we can iterate through the current processes*/
	struct task_struct *task;

	/*Deaclare a sysfs_message and allocate memery for it on the kernel*/
	struct sysfs_message *message =  kmalloc(sizeof(struct sysfs_message), GFP_KERNEL);
		

	/*Retrieve the data from the user space and store it in the newly declared message*/
	if(copy_from_user(message, (void *)*(int *)buf, sizeof(struct sysfs_message))) {
 	    return -EFAULT;
	}
	
	/*Iterate through the processes*/
	for_each_process(task) {
	    /* For the task that has the same pid as the pid we retrieved from the user space,
 	     * we update the message with the information from that task */
	    if(task->pid == message->pid) {
		strcpy(message->address->comm, task->comm);
		message->address->state = task->state;
		message->address->pid = task->pid;
	    }

	}	

	/*Finally we send the data back to the user space we the updated data*/
	if(copy_to_user((void *)*(int *)buf, message, sizeof(struct sysfs_message))) {
	    return -EFAULT;
	}
	
	/*Free the message pointer*/
	kfree(message);
  	return count;
}

static struct kobj_attribute pid_attribute =
	__ATTR(pid, 0220, NULL, pid_write);

/* Setup  */
static struct attribute *attrs[] = {
	&kcurrent_attribute.attr,
	&pid_attribute.attr,
	NULL,
};
static struct attribute_group attr_group = {
	.attrs = attrs,
};

static struct kobject *kernellab_kobj;

static int __init kernellab_init(void)
{

	/*Declare the needed variables */
	struct task_struct *task;
	int retval;

	/*The message that prints out when we inject the module */
	printk("kernellab module INJECTED\n");

	/*Iterate through the processes */
	for_each_process(task) {
	   /*If the pid is equal to 1 (the init process pid), we print out the command */
	   if(task->pid == 1) {
		printk("%s\n", task->comm);
	   }
	
	}

	kernellab_kobj = kobject_create_and_add("kernellab", kernel_kobj);
	if (!kernellab_kobj)
		return -ENOMEM;

	retval = sysfs_create_group(kernellab_kobj, &attr_group);
	if (retval) 
		kobject_put(kernellab_kobj);
	return retval;
}

static void __exit kernellab_exit(void)

{
	/*The text that prints out when we remove the module form the kernel */
	printk("kernellab module UNLOADED\n");

	kobject_put(kernellab_kobj);
}

module_init(kernellab_init);
module_exit(kernellab_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Hilmar Ragnarsson <hilmarr13@ru.is>");
MODULE_AUTHOR("Sindri Már Kaldal Sigurjónsson <sindrik13@ru.is>");

